import { createStore } from 'redux'
import reducer from '../form/reducer'

const store = createStore(reducer)

export default store