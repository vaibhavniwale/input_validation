import { create_element,update,mark_complete,formUpdate } from './formType'
const { v4: uuidv4 } = require('uuid');

 const initialState ={
    formComponents: [
    ],
    formname:''
  }

  const reducer = (state = initialState, action)=>{
      switch(action.type){
          case create_element:{
            let element = action.componentType
            let comp
            if(element==='Text'){
              comp = { id:uuidv4() , type:element,placeholder:undefined,maxLength:undefined ,created:false }
            }else if(element==='Number'){
              comp = { id:uuidv4() , type:element, placeholder:undefined,min:undefined, max:undefined ,created:false }
            }else {
              comp = { id:uuidv4() , type:element, placeholder:undefined, created:false}
            }
            return {
                ...state,
                formComponents:[...state.formComponents,comp]
            }
          }
          case update:{
            return {
                ...state,
                formComponents:state.formComponents.map((element)=>{
                    if(element.id === action.id){
                      element[action.componentUpdateName] = action.componentUpdateValue
                    }
                    return element
                  })
            }
          }
          case mark_complete:{
            return {
                ...state,
                formComponents:state.formComponents.map((element)=>{
                    if(element.id === action.id){
                        element.created = true
                    }
                    return element
                  })
            }
          }
          case formUpdate:{
            console.log(state)
            return {
              ...state,
              formname:action.updateValue
          }
          } 
          default: return state
      }
  } 

  export default reducer