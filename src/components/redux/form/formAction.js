import { create_element,update,mark_complete,formUpdate } from './formType'

export const createElement = (element) =>{
    return{
        type:create_element,
        componentType:element
    }
}

export const updated = (id,element) =>{
    return{
        type:update,
        id: id,
        componentUpdateName:element.target.name,
        componentUpdateValue: element.target.value
    }
}

export const markComplete = (id) =>{
    return{
        type:mark_complete,
        id: id
    }
}

export const formna = (element) =>{
    return{
        type:formUpdate,
        updateValue: element.target.value
    }
}