import React from 'react'
import Header from './Header'
import ButtonComponents from './ButtonComponents'
import Addcomponent from './Addcomponent'
import { createElement,updated,markComplete,formna } from './redux/form/formAction'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'


let itemNames = [
    'Text',
    'Number',
    'Email',
    'Password',
    'Radio',
    'Checkbox',
    'Textarea',
    'Drop-down',
  ]
  

function Home(props) {
    if(props.formComponents.length !==0){
        let a = props.formComponents.filter((element)=>{
            return !element.created
        })
        if(a.length === 0){
            return (
                <div>
                <Header formname={props.formname}/>
                  <div className='row'>
                        <div className='column sidebar'>
                          Click the field you want to add
                          <ButtonComponents name={itemNames} createElement={props.createElement} />
                        </div>
                        <div className='column formcomponent'>
                         <div className='column'>
                            <Addcomponent 
                            items={props.formComponents}
                            textfunction={[props.createdtextElement,props.updateplaceholder]}
                            />
                         </div>
                         <Link to='/form'><button className='menuButtonreverse'>Create</button></Link>
                        </div>
                      </div>
                </div>
              )
        }
    }
  return (
    <div>
    <Header formname={props.formname}/>
      <div className='row'>
            <div className='column sidebar'>
              Click the field you want to add
              <ButtonComponents name={itemNames} createElement={props.createElement}/>
            </div>
            <div className='column formcomponent'>
             <div className='column'>
                <Addcomponent 
                items={props.formComponents}
                textfunction={[props.createdtextElement,props.updateplaceholder]}
                />
             </div>
            </div>
          </div>
    </div>
  )
}


const mapStateTOProps = (state) =>{
    return{
      formComponents: state.formComponents,
      formname: state.formname
    }
  }
  
  const mapDispachTOProps = (dispach) =>{
    return{
      createElement: (element) => dispach(createElement(element)),
      updateplaceholder: (id,element)=> dispach(updated(id,element)),
      createdtextElement: (id)=> dispach(markComplete(id)),
      formna: (element)=>dispach(formna(element))
    }
  }
  
  export default connect(mapStateTOProps,mapDispachTOProps)(Home)