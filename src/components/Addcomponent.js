import React from 'react'
import InputComponent from './InputComponent'

export default function Addcomponent(props) {
  return props.items.map((element)=>{
    return <InputComponent key={element.id} item={element} textfunction={props.textfunction}/>
  })
}
