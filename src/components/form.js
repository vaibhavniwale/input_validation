import React from 'react'
import { connect } from 'react-redux'
import Header from './Header'

import Addcomponent from './Addcomponent'

function form(prop) {
    console.log(prop)
  return (
    <div>
      <Header formname={prop.formname}/>
      <Addcomponent 
                items={prop.formComponents}
                />
    </div>
  )
}

const mapStateTOProps = (state) =>{
  return{
    formComponents: state.formComponents,
    formname: state.formname
  }
}


export default connect(mapStateTOProps)(form)