import React from 'react'

export default function Number(props) {
  if(props.element.created===false){
    return (
      <div className='tile'>
        <p>Enter the placeholder</p>
        <input type='text' name='placeholder' defaultValue={props.element.placeholder} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <p>Enter the min</p>
        <input type='number' name='min' defaultValue={props.element.min} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <p>Enter the max</p>
        <input type='number' name='max'  defaultValue={props.element.max} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <button className='createbutton' onClick={props.func[0].bind(this,props.element.id)}>create element</button>
      </div>
    )
  }else{
    return (
      <div className='tile'>
        <input type={props.element.type} placeholder={props.element.placeholder} max={props.element.max} min={props.element.min}/>
      </div>
    )
  }
  
}