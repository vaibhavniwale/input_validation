import React from 'react'

export default function Text(props) {
  if(props.element.created===false){
    return (
      <div className='tile'>
        <p>Enter the name of field</p>
        <input type='text' name='placeholder' defaultValue={props.element.placeholder} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <p>Enter the max length of field</p>
        <input type='number' name='maxLength' defaultValue={props.element.maxLength} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <button className='createbutton' onClick={props.func[0].bind(this,props.element.id)}>create element</button>
      </div>
    )
  }else{
    return (
      <div className='tile'>
        <input type={props.element.type} placeholder={props.element.placeholder} maxLength={props.element.maxLength}/>
      </div>
    )
  }
  
}
