import React from 'react'

export default function Radioelement(props) {
  return (
    <div>
     <input type={props.type} id={props.name} name={props.name}/>
    <label >{props.name}</label>
    </div>
  )
}
