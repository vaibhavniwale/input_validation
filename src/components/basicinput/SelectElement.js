import React from 'react'

export default function SelectElement(props) {
  return (<option defaultValue={props.name}>{props.name}</option>)
}
