import React from 'react'
import Radioelement from './Radioelement'

export default function Checkbox(props) {
  if(props.element.created===false){
    return (
      <div className='tile'>
        <p>Enter the values like male,female</p>
        <input type='text' name='placeholder' defaultValue={props.element.placeholder} onChange={props.func[1].bind(this,props.element.id)}/>
        <span className="validity"></span>
        <button className='createbutton' onClick={props.func[0].bind(this,props.element.id)}>create element</button>
      </div>
    )
  }else{

    return props.element.placeholder.split(',').map((e)=>{
        return <Radioelement key={props.element.id+e} name={e} type={props.element.type}/>
    })
  }
  
}
