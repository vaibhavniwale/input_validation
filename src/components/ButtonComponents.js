import React from 'react'
import Buttoncomponent from './Buttoncomponent'

export default function ButtonComponents(props) {
  return props.name.map((element)=>{
      return <Buttoncomponent key={element} name={element} createElement={props.createElement}/>
  })
}
