import React from 'react'
import Header from './Header'
import { connect } from 'react-redux'
import { formna } from './redux/form/formAction'
import { Link } from 'react-router-dom'


function askFormname(props) {
  return (
    <div className='center'>
         <Header formname={'Form Maker'}/>
        <h1>Want to make a form Enter the form name below</h1>
        <input className='formname' type='text' defaultValue={props.formname} onChange={props.formna.bind(this)} placeholder='Enter the form name'/>
        {console.log(props.formname)}
        <span></span>
        <Link to='/create'>hi</Link>
    </div>
  )
}


const mapStateTOProps = (state) =>{
    return{
      formname: state.formname
    }
  }
  
  const mapDispachTOProps = (dispach) =>{
    return{
      formna: (element)=>dispach(formna(element))
    }
  }
  
  export default connect(mapStateTOProps,mapDispachTOProps)(askFormname)
