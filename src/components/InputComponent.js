import React from 'react'
import Text from './basicinput/Text'
import Number from './basicinput/Number'
import Email from './basicinput/Email'
import Password from './basicinput/Password'
import Radio from './basicinput/Radio'
import Checkbox from './basicinput/Checkbox'
import Textarea from './basicinput/Textarea'
import Dropdown from './basicinput/Dropdown'

export default function InputComponent(props) {
    if(props.item.type === 'Text'){
        return <Text element={props.item} func={props.textfunction}/> 
    }else if(props.item.type === 'Number'){
        return <Number element={props.item} func={props.textfunction}/> 
    }else if(props.item.type === 'Email'){
        return <Email element={props.item} func={props.textfunction}/>
    }else if(props.item.type === 'Password'){
        return <Password element={props.item} func={props.textfunction}/> 
    }else if(props.item.type === 'Radio'){
        return <Radio element={props.item} func={props.textfunction}/>
    }else if(props.item.type === 'Checkbox'){
        return <Checkbox element={props.item} func={props.textfunction}/>
    }else if(props.item.type === 'Textarea'){
        return <Textarea element={props.item} func={props.textfunction}/>
    }else if(props.item.type === 'Drop-down'){
        return <Dropdown element={props.item} func={props.textfunction}/>
    }else{
        return(<div></div>)
    }
}
