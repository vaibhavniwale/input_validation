import React from 'react'

export default function Buttoncomponent(props) {
   return (
    <button onClick={props.createElement.bind(this,props.name)} className='menuButton' type='button'>{props.name}</button>
  )
}
