import React from 'react'
import { BrowserRouter,Route } from 'react-router-dom'
import './App.css';
import Form from './components/form'
import Home from './components/Home';


import { Provider } from 'react-redux'
import store from './components/redux/store/store'
import askFormname from './components/askFormname';

function App(props){
    return (
      <Provider store={store}>
      <BrowserRouter>
      <div className="App">
        <Route exact path='/' component={askFormname}/>
        <Route path='/create' component={Home}/>
        <Route path='/form' component={Form}/>
      </div>
    </BrowserRouter>
    </Provider>
    )
}


export default App;
